
import React, { Fragment } from 'react';
import './App.css';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import ReduxThunk  from 'redux-thunk';
import { Provider } from 'react-redux'; // responsible to make react and redux connection
import { composeWithDevTools } from 'redux-devtools-extension';
import UsersReducer from './store/reducers/users-reducer';
import UserComponent from './components/UserComponent';
import UserDetailsComponent from './components/UserDetailsComponent';

const rootReducer = combineReducers({
  usersState: UsersReducer,
});

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(ReduxThunk),
  // other store enhancers if any
));


function App() {
  return (
    <Provider store={store}>
       <Fragment>
       <div className='container'> 
          <UserComponent/>
          <UserDetailsComponent/>
        </div>
       </Fragment>
    </Provider>
  );
}

export default App;
