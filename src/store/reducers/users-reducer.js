import { GET_USERS, SHOW_USER_DETAILS, CLEAR_USER_SELECTED } from '../actions/users-action'

const initialState = {
    users: [],
    showModal: false,
    userDetails: []

};

const UsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USERS:
            return{
                ...state,
                users: action.users
            }
        
        case SHOW_USER_DETAILS:
            return{
                ...state,
                showModal: true,
                userDetails: action.userDetails
            }
        case CLEAR_USER_SELECTED:
            return {
                ...state,
                userDetails: [],
                showModal: false
            }
        default:
            return state;;
    }
};

export default UsersReducer;