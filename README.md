# README #
Creating a project based on react JS and redux thunk with the below requirements:

### Requirements ###

1. Consuming the data using the API https://randomuser.me/.
2. Displaying the data in a list, using a list.
3. When the user hits a list item he must go to a list of details to show more
information about the selected user.
4. How to work with bootstrap and react https://react-bootstrap.github.io/components/modal/