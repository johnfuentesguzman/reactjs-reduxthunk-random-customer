import React, { useEffect, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as userActions from '../store/actions/users-action';
import UserDetailsComponent from './UserDetailsComponent';

const UserComponent = () => {
    const dispatch = useDispatch();
    const [showDetails, setShowDetails] = useState(false);
    const [userSelected, setUserSelected] = useState(null);

    const users = useSelector((state) => state.usersState.users); // see app.js:  const rootReducer = combineReducers({
    const loadUsers = useCallback( async () => {
        await dispatch(userActions.getUsers()); // it returns a promisse¡
    
    },[dispatch]);

    const goToDetails = useCallback(async (user) => {
        try {
            await dispatch(userActions.userDetails(user));
            setShowDetails(true);
            setUserSelected(user);
        } catch (error) {
            console.log(error);
        }
        
    }, [showDetails, userSelected]);

    useEffect(() => {
        loadUsers();
    }, [ loadUsers]);

    return(
        <div className="App">
            <div className="row">
            {users !== null  && 
                users.map((user, index) =>
                    <div className="col-sm-6" key={index}>
                        <div className="card">
                            <img src={user.picture.medium} className="card-img-top" alt="..."/>
                            <div className="card-body">
                                <h5 className="card-title">{user.name.title}. {user.name.first} {user.name.last} </h5>
                                <p className="card-text">{user.email}</p>
                            </div>
                            <button 
                             type="button"
                             onClick={() => goToDetails(user, index)}
                             className="btn btn-primary">See Details</button>
                        </div>
                    </div>
                )
            }
            { showDetails && userSelected !== null &&
                <UserDetailsComponent/>
            }
            </div>
        </div>
    )
}
export default UserComponent;
