export const GET_USERS = 'GET_USERS';
export const SHOW_USER_DETAILS = 'SHOW_USER_DETAILS';
export const CLEAR_USER_SELECTED = 'CLEAR_USER_SELECTED';

export const getUsers = (pagination = 20) => {
    return async dispatch => {// this dispatch is for redux-thunk its the funcition that this middleware needs as parameter
      try {
      const response = await fetch(
        `https://randomuser.me/api/?results=${pagination}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
      if (!response.ok) {
        throw new Error('Opps, something was wrong')
      }

      const usersFromAPI = await response.json();
        dispatch({ 
          type: GET_USERS,
          users: usersFromAPI.results,
        });
      } catch(error){
        throw error;
      } 
    };
  };

  export const userDetails = (user) => {
    return async dispatch => {
      try {
        dispatch({ 
          type: SHOW_USER_DETAILS,
          userDetails : user
        });
        
      } catch (error) {
        throw error;
      }
    }
  };

  export const clearUserSelected = () =>{
    return async dispatch => {
      try {
        dispatch({
          type: CLEAR_USER_SELECTED
        });
      } catch (error) {
        throw error;
      }
    }
  };