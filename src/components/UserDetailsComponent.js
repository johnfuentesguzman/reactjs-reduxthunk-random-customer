
import React, {useState, useEffect} from 'react';
import { Modal, Button, ListGroup, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import * as userActions from '../store/actions/users-action'; 
export const UserDetailsComponent = (props) =>{
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.usersState.userDetails);
  const showModal = useSelector((state) => state.usersState.showModal);
  const [openModal, setOpenModal] = useState(showModal);
  const [userData, setUserData] = useState(null);

  const handleClose = () => {
    dispatch(userActions.clearUserSelected());
  };

  useEffect(() => {
    setOpenModal(showModal);
    setUserData(userDetails);
  }, [showModal]); //  every single time when this component is caññ

  return(        
    <Modal
      animation={false}
      id="user-details-modal"
      show={openModal}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      >
      <Modal.Header closeButton>
      <Modal.Title>User Information</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        { userData && userData !== null && userData.name && 
          <ListGroup>
            <ListGroup.Item><h5>Name:</h5> {userData.name.title}. {userData.name.first} {userData.name.last}</ListGroup.Item>
            <ListGroup.Item><h5>Email:</h5> {userData.email}</ListGroup.Item>
            <ListGroup.Item><h5>Phone:</h5> {userData.phone}</ListGroup.Item>
            <ListGroup.Item><h5>Country:</h5> {userData.location.country}</ListGroup.Item>
            <ListGroup.Item><h5>Gender:</h5> {userData.gender}</ListGroup.Item>
          </ListGroup>
        }
        </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose} >
              Close
            </Button>
          </Modal.Footer>
    </Modal>
  )
};

export default UserDetailsComponent;